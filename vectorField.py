import numpy as np
import matplotlib.pyplot as plt

# Create grid
nx, ny = 15, 15
x = np.linspace(0,1,nx)
y = np.linspace(0,1,ny)
X, Y = np.meshgrid(x,y)
gx = np.zeros((nx,ny))
gy = -9.81*np.ones((nx,ny))
plt.figure()
plt.subplot(111, aspect='equal')
plt.quiver(X,Y,gx,gy)
plt.xlabel('Distance')
plt.ylabel('Height')
plt.show()
